package com.androidegitim.sifrettutma;

import android.app.Application;

import com.androidegitim.androidegitimlibrary.helpers.RDALogger;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class SifreTutmaApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        startRealm();

        initializeRDALogger();
    }

    private void initializeRDALogger() {

        RDALogger.start(getString(R.string.app_name)).enableLogging(true);
    }

    private void startRealm() {

        //https://realm.io/docs/get-started/android-demo-app/
        //https://www.androidhive.info/2016/05/android-working-with-realm-database-replacing-sqlite-core-data/

        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(this)
                .name(Realm.DEFAULT_REALM_NAME)
                .schemaVersion(0)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(realmConfiguration);
    }
}
