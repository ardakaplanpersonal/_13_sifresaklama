package com.androidegitim.sifrettutma.constants;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public final class PassingDataKeyConstants {

    public static final String PASSWORD_ID = "PASSWORD_ID";

    private PassingDataKeyConstants() {

    }
}
