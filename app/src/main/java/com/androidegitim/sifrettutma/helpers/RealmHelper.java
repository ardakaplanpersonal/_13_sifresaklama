package com.androidegitim.sifrettutma.helpers;

import java.util.Map;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Created by Arda Kaplan
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class RealmHelper {

    private static Realm realm;

    public static Realm getRealm() {

        if (realm == null) {

            realm = Realm.getDefaultInstance();
        }

        return realm;
    }

    @Deprecated
    private static <R extends RealmObject> long getNextID(Class<R> clazz) {

        if (hasObject(clazz)) {

            return (getRealm().where(clazz).maximumInt("ID") + 1);

        } else {

            return 0;
        }
    }


    public static <R extends RealmObject> boolean hasObject(Class<R> clazz) {

        return !getRealm().allObjects(clazz).isEmpty();
    }

    public static void saveObject(RealmObject objectToSave) {

        Realm realm = getRealm();
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(objectToSave);
        realm.commitTransaction();
    }

    public static <R extends RealmObject> void deleteAll(Class<R> clazz) {

        Realm realm = getRealm();

        RealmResults<R> results = realm.where(clazz).findAll();

        realm.beginTransaction();
        results.clear();
        realm.commitTransaction();
    }

    public static <R extends RealmObject> void deleteWithQuery(Class<R> clazz, String fieldName, int value) {

        Realm realm = getRealm();

        RealmResults<R> results = realm.where(clazz).equalTo(fieldName, value).findAll();

        for (int i = 0; i < results.size(); i++) {

            realm.beginTransaction();
            results.clear();
            realm.commitTransaction();
        }
    }

    public static <R extends RealmObject> void deleteWithQuery(Class<R> clazz, String fieldName, long value) {

        Realm realm = getRealm();

        RealmResults<R> results = realm.where(clazz).equalTo(fieldName, value).findAll();

        for (int i = 0; i < results.size(); i++) {

            realm.beginTransaction();
            results.clear();
            realm.commitTransaction();
        }
    }

    public static <R extends RealmObject> R getSingleObject(Class<R> clazz, String fieldName, int value) {

        return getRealm().where(clazz).equalTo(fieldName, value).findFirst();
    }

    public static <R extends RealmObject> R getSingleObject(Class<R> clazz, String fieldName, long value) {

        return getRealm().where(clazz).equalTo(fieldName, value).findFirst();
    }

    public static <R extends RealmObject> RealmResults<R> getAllWithQuery(Class<R> clazz, Map<String, Object> equalMap) {

        RealmQuery realmQuery = getRealm().where(clazz);

        for (Map.Entry<String, Object> entry : equalMap.entrySet()) {

            if (entry.getValue() instanceof Integer) {

                realmQuery = realmQuery.equalTo(entry.getKey(), (Integer) entry.getValue());
            }
        }

        return realmQuery.findAll();
    }

    public static <R extends RealmObject> RealmResults<R> getAllWithQuery(Class<R> clazz, String fieldName, String value) {

        return getRealm().where(clazz).equalTo(fieldName, value).findAll();
    }

    public static <R extends RealmObject> RealmResults<R> getAllWithQuery(Class<R> clazz, String fieldName, int value) {

        return getRealm().where(clazz).equalTo(fieldName, value).findAll();
    }


    public static <R extends RealmObject> RealmResults<R> getAllWithQueryAndSort(Class<R> clazz, String fieldName, int value, String sortingField, boolean sortAscending) {

        return getRealm().where(clazz).equalTo(fieldName, value).findAllSorted(sortingField, sortAscending);
    }

    public static <R extends RealmObject> RealmResults<R> getAllWithQuery(Class<R> clazz, String fieldName, long value) {

        return getRealm().where(clazz).equalTo(fieldName, value).findAll();
    }

    public static <R extends RealmObject> RealmResults<R> getAll(Class<R> clazz) {

        return getRealm().allObjects(clazz);
    }

    public static <R extends RealmObject> RealmResults<R> getAllAndSort(Class<R> clazz, String sortingField, boolean sortAscending) {

        return getRealm().where(clazz).findAllSorted(sortingField, sortAscending);
    }
}