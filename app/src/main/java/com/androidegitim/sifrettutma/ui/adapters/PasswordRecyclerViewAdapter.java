package com.androidegitim.sifrettutma.ui.adapters;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidegitim.sifrettutma.R;
import com.androidegitim.sifrettutma.constants.PassingDataKeyConstants;
import com.androidegitim.sifrettutma.models.Password;
import com.androidegitim.sifrettutma.ui.activities.PasswordActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class PasswordRecyclerViewAdapter extends RecyclerView.Adapter<PasswordRecyclerViewAdapter.PasswordViewHolder> {

    private Activity activity;
    private List<Password> passwordArrayList;

    public PasswordRecyclerViewAdapter(Activity activity, List<Password> passwordArrayList) {
        this.activity = activity;
        this.passwordArrayList = passwordArrayList;
    }

    @Override
    public PasswordViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        View rowView = layoutInflater.inflate(R.layout.row_layout_password, parent, false);

        return new PasswordViewHolder(rowView);
    }

    @Override
    public void onBindViewHolder(PasswordViewHolder holder, int position) {

        final Password password = passwordArrayList.get(position);

        holder.domainNameTextView.setText(password.getDomainName());

        holder.rootLinearLayout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent intent = new Intent(activity, PasswordActivity.class);

                intent.putExtra(PassingDataKeyConstants.PASSWORD_ID, password.getID());

                activity.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return passwordArrayList.size();
    }

    class PasswordViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.row_layout_password_linearlayout_root)
        LinearLayout rootLinearLayout;
        @BindView(R.id.row_layout_password_textview_domain_name)
        TextView domainNameTextView;

        private PasswordViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }
    }
}
