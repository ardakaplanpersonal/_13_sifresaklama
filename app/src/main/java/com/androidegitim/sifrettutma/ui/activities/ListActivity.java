package com.androidegitim.sifrettutma.ui.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.androidegitim.androidegitimlibrary.helpers.RDALogger;
import com.androidegitim.sifrettutma.R;
import com.androidegitim.sifrettutma.helpers.RealmHelper;
import com.androidegitim.sifrettutma.models.Password;
import com.androidegitim.sifrettutma.ui.adapters.PasswordRecyclerViewAdapter;

import butterknife.BindView;
import butterknife.OnClick;

public class ListActivity extends BaseActivity {

    @BindView(R.id.list_recyclerview_passwords)
    RecyclerView passwordsRecyclerView;
    @BindView(R.id.list_textview_empty_text)
    TextView emptyTextView;

    private PasswordRecyclerViewAdapter passwordRecyclerViewAdapter;

    @SuppressLint("MissingSuperCall")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.activity_list);

        fillList();
    }

    @Override
    protected void onResume() {
        super.onResume();

        passwordRecyclerViewAdapter.notifyDataSetChanged();

        checkList();
    }

    @OnClick(R.id.list_button_create_new)
    public void createNew() {

        Intent intent = new Intent(ListActivity.this, PasswordActivity.class);

        startActivity(intent);
    }

    private void fillList() {

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        passwordsRecyclerView.setLayoutManager(linearLayoutManager);


        RDALogger.info(RealmHelper.getAll(Password.class));

        passwordRecyclerViewAdapter = new PasswordRecyclerViewAdapter(this, RealmHelper.getAll(Password.class));

        passwordsRecyclerView.setAdapter(passwordRecyclerViewAdapter);
    }

    private void checkList() {

        if (RealmHelper.getAll(Password.class).size() == 0) {

            emptyTextView.setVisibility(View.VISIBLE);

        } else {

            emptyTextView.setVisibility(View.GONE);
        }
    }
}
