package com.androidegitim.sifrettutma.ui.activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;

import butterknife.ButterKnife;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public abstract class BaseActivity extends Activity {

    protected void onCreate(@Nullable Bundle savedInstanceState, int layoutID) {
        super.onCreate(savedInstanceState);

        setContentView(layoutID);

        ButterKnife.bind(this);
    }
}
