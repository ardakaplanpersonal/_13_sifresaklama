package com.androidegitim.sifrettutma.ui.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.widget.Button;
import android.widget.Toast;

import com.androidegitim.androidegitimlibrary.helpers.RDALogger;
import com.androidegitim.sifrettutma.R;
import com.androidegitim.sifrettutma.models.Password;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class LoginActivity extends BaseActivity {

    private static final int PASSWORD = 1234;

    @BindView(R.id.login_button_1)
    Button button1;
    @BindView(R.id.login_button_2)
    Button button2;
    @BindView(R.id.login_button_3)
    Button button3;
    @BindView(R.id.login_button_4)
    Button button4;
    @BindView(R.id.login_button_5)
    Button button5;
    @BindView(R.id.login_button_6)
    Button button6;
    @BindView(R.id.login_button_7)
    Button button7;
    @BindView(R.id.login_button_8)
    Button button8;
    @BindView(R.id.login_button_9)
    Button button9;

    private String userInputForPassword = "";

    CountDownTimer countDownTimer = new CountDownTimer(2000, 1000) {

        @Override
        public void onTick(long millisUntilFinished) {

        }

        @Override
        public void onFinish() {

            RDALogger.info(" userInputForPassword sıfırlandı");

            userInputForPassword = "";
        }

    }.start();

    @SuppressLint("MissingSuperCall")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.activity_login);
    }

    private void setPassword(String number) {

        countDownTimer.cancel();

        userInputForPassword = userInputForPassword + number;

        if (userInputForPassword.length() == 4) {

            int userInput = Integer.valueOf(userInputForPassword);

            if (userInput == PASSWORD) {

                Intent intent = new Intent(LoginActivity.this, ListActivity.class);

                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                startActivity(intent);

                return;

            } else {

                userInputForPassword = "";

                Toast.makeText(getApplicationContext(), "Yanlış Dizayn", Toast.LENGTH_SHORT).show();
            }

        }

        countDownTimer.start();
    }

    @OnClick(R.id.login_button_1)
    public void button1() {

        setPassword(button1.getTag().toString());
    }

    @OnClick(R.id.login_button_2)
    public void button2() {

        setPassword(button2.getTag().toString());
    }

    @OnClick(R.id.login_button_3)
    public void button3() {

        setPassword(button3.getTag().toString());
    }

    @OnClick(R.id.login_button_4)
    public void button4() {

        setPassword(button4.getTag().toString());
    }

    @OnClick(R.id.login_button_5)
    public void button5() {

        setPassword(button5.getTag().toString());
    }

    @OnClick(R.id.login_button_6)
    public void button6() {

        setPassword(button6.getTag().toString());
    }

    @OnClick(R.id.login_button_7)
    public void button7() {

        setPassword(button7.getTag().toString());
    }

    @OnClick(R.id.login_button_8)
    public void button8() {

        setPassword(button8.getTag().toString());
    }

    @OnClick(R.id.login_button_9)
    public void button9() {

        setPassword(button9.getTag().toString());
    }
}
