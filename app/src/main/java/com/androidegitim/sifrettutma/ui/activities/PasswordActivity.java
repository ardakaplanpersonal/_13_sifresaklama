package com.androidegitim.sifrettutma.ui.activities;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.androidegitim.androidegitimlibrary.helpers.DialogHelpers;
import com.androidegitim.androidegitimlibrary.helpers.RDALogger;
import com.androidegitim.sifrettutma.R;
import com.androidegitim.sifrettutma.constants.PassingDataKeyConstants;
import com.androidegitim.sifrettutma.helpers.RealmHelper;
import com.androidegitim.sifrettutma.models.Password;

import butterknife.BindView;
import butterknife.OnClick;
import io.realm.Realm;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class PasswordActivity extends BaseActivity {

    @BindView(R.id.password_edittext_domain_name)
    EditText domainNameEditText;
    @BindView(R.id.password_edittext_e_mail)
    EditText eMailEditText;
    @BindView(R.id.password_edittext_password)
    EditText passwordEditText;
    @BindView(R.id.password_edittext_username)
    EditText usernameEditText;
    @BindView(R.id.password_button_delete)
    Button deleteButton;
    @BindView(R.id.password_button_edit)
    Button editButton;
    @BindView(R.id.password_button_save)
    Button saveButton;

    Password password;

    @SuppressLint("MissingSuperCall")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.activity_password);

        if (getIntent().hasExtra(PassingDataKeyConstants.PASSWORD_ID)) {

            password = RealmHelper.getSingleObject(Password.class, "ID", getIntent().getExtras().getLong(PassingDataKeyConstants.PASSWORD_ID));

            editButton.setVisibility(View.VISIBLE);

            saveButton.setVisibility(View.GONE);

            adjustEdittext(false);

            setViews();
        }
    }

    private void adjustEdittext(boolean adjust) {

        domainNameEditText.setEnabled(adjust);
        eMailEditText.setEnabled(adjust);
        usernameEditText.setEnabled(adjust);
        passwordEditText.setEnabled(adjust);
    }

    private void setViews() {

        domainNameEditText.setText(password.getDomainName());
        eMailEditText.setText(password.geteMail());
        usernameEditText.setText(password.getUsername());
        passwordEditText.setText(password.getPassword());
    }

    @OnClick(R.id.password_button_save)
    public void save() {

        if (password == null) {

            password = new Password();

            password.setID(System.currentTimeMillis());
            RDALogger.info(System.currentTimeMillis());
        }

        Realm realm = RealmHelper.getRealm();

        realm.beginTransaction();

        password.setDomainName(domainNameEditText.getText().toString().trim());
        password.seteMail(eMailEditText.getText().toString().trim());
        password.setUsername(usernameEditText.getText().toString().trim());
        password.setPassword(passwordEditText.getText().toString().trim());

        realm.copyToRealmOrUpdate(password);
        realm.commitTransaction();

        DialogHelpers.showDialog(this, "Bilgi", "Şifre başarıyla kaydedildi.", "Tamam", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                onBackPressed();

            }
        }, null, null, null, null);

    }

    @OnClick(R.id.password_button_edit)
    public void editFields() {

        editButton.setVisibility(View.GONE);

        deleteButton.setVisibility(View.VISIBLE);

        saveButton.setVisibility(View.VISIBLE);

        adjustEdittext(true);
    }


    @OnClick(R.id.password_button_delete)
    public void delete() {

        DialogHelpers.showDialog(this, "Uyarı", "Şifreyi silmek istediğinize emin misiniz?", "Sil", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                RealmHelper.deleteWithQuery(Password.class, "ID", password.getID());

                DialogHelpers.showDialog(PasswordActivity.this, "Bilgi", "Şifre başarıyla silindi.", "Tamam", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        onBackPressed();

                    }
                }, null, null, null, null);


            }
        }, "İptal", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
            }
        }, null, null);


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        RDALogger.info("OnBackPressed çağrıldı");
    }
}
